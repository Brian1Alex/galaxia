<?php
class Planeta extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function insertar($datos)
    {
        return $this->db->insert("planeta", $datos);
    }

    function borrar($id_pla_recup_ba)
        {
            $this->db->where("id_pla_recup_ba", $id_pla_recup_ba);
            return $this->db->delete("planeta");
        }

    function obtenerPla()
    {
        $listPla = $this->db->get("planeta");
        if ($listPla->num_rows() > 0) {
            return $listPla->result();
        } else {
            return false;
        }
    }

    function obteGala()
    {
        $listGala = $this->db->get("galaxia");
        if ($listGala->num_rows() > 0) {
            return $listGala->result();
        } else {
            return false;
        }
    }

    function editar($id_pla_recup_ba, $dat)
        {
            $this->db->where("id_pla_recup_ba", $id_pla_recup_ba);
            return $this->db->update('planeta', $dat);
        }

        function obtenerPorId($id_pla_recup_ba)
        {
            $this->db->where("id_pla_recup_ba", $id_pla_recup_ba);
            $planeta = $this->db->get("planeta");
            if ($planeta->num_rows() > 0) {
                return $planeta->row();
            }
            return false;
        }

}
