<?php
    class Galaxia extends CI_Model
    {
        function __construct()
        {
            parent::__construct();
        }

        function insertar($datos)
        {
            return $this->db->insert("galaxia", $datos);
        }

        function obtenerGala()
        {
            $listGala = $this->db->get("galaxia");
            if ($listGala->num_rows() > 0) {
                return $listGala->result();
            }else{
                return false;
            }
        }

        function borrar($id_recup_ba)
        {
            $this->db->where("id_recup_ba", $id_recup_ba);
            return $this->db->delete("galaxia");
        }

        function editar($id_recup_ba, $dat)
        {
            $this->db->where("id_recup_ba", $id_recup_ba);
            return $this->db->update('galaxia', $dat);
        }

        function obtenerPorId($id_recup_ba)
        {
            $this->db->where("id_recup_ba", $id_recup_ba);
            $galaxia = $this->db->get("galaxia");
            if ($galaxia->num_rows() > 0) {
                return $galaxia->row();
            }
            return false;
        }


        
    }