<h1 Align="center">Agregar nueva galaxia</h1>

<form class="" id="frm_nueva_galaxia" action="<?php echo site_url(); ?>/Galaxias/guardaGala" method="post">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <label for="">Nombre: <span class="obligatorio">(Campo Requerido)</span></label>
                <br>
                <input type="text" placeholder="Ingresar nombre de la galaxia" class="form-control" name="nom_recup_ba" required value="">
            </div>
            <div class="col-md-6">
                <label for=""> Descripción: <span class="obligatorio">(Campo Requerido)</span></label>
                <br>
                <input type="text" placeholder="Ingresar una Descripcion de la galaxia" class="form-control" required name="descrip_recup_ba" value="">
                <br>
            </div>

            <br>
            <div class="col-md-12 text-center">
                <button type="submit" name="button" class="btn btn-primary">
                    GUARDAR
                </button>
                &nbsp;
                <a href="<?php echo site_url(); ?>/galaxias/lisgala" class="btn btn-danger">CANCELAR</a>
            </div>

</form>

<script type="text/javascript">
    $("#frm_nueva_galaxia").validate({
        rules: {
            nom_recup_ba: {
                required: true,
                minlength: 3,
                maxlength: 50,
            },
            descrip_recup_ba: {
                required: true,
                minlength: 3,
                maxlength: 200,
                letras: true,
            }
        },
        messages: {
            nom_recup_ba: {
                required: "Por favor ingresar un nombre",
                minlength: "El nombre debe tener al menos 3 caracteres",
                maxlength: "Nombre incorrecto",

            },
            descrip_recup_ba: {
                required: "Por favor ingrese una descripcion",
                minlength: "La descripcion debe tener al menos 3 caracteres",
                maxlength: "Descripcion incorrecta",
            }
        }
    });
</script>