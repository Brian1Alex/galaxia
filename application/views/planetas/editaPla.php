<h1 Align="center">Editar Planeta</h1>

<form class="" id="frm_editar_planeta" action="<?php echo site_url(); ?>/Planetas/editarPla" method="post">
    <div class="container">
        <div class="row">
            <input type="text" class="form-control" name="id_pla_recup_ba" id="id_pla_recup_ba" hidden value="<?php echo $editaPlan->id_pla_recup_ba; ?>">
            <div class="col-md-4">
                <label for="">Nombre: <span class="obligatorio">(Campo Requerido)</span></label>
                <br>
                <input type="text" placeholder="Ingresar nombre del Planeta" class="form-control" name="nom_pla_recup_ba" required value="<?php echo $editaPlan->nom_pla_recup_ba; ?>">
            </div>
            <div class="col-md-4">
                <label for=""> Orden: <span class="obligatorio">(Campo Requerido)</span></label>
                <br>
                <input type="number" placeholder="Ingresar el orden del Planeta" class="form-control" required name="orden_pla_recup_ba" value="<?php echo $editaPlan->orden_pla_recup_ba; ?>">
                <br>
            </div>
            <div class="col-md-4">
                <label for=""> Foto: <span class="obligatorio">(Campo Requerido)</span></label>
                <br>
                <input type="text" placeholder="Ingresar una Descripcion de la galaxia" class="form-control" name="foto_pla_recup_ba" value="<?php echo $editaPlan->foto_pla_recup_ba; ?>">
                <br>
            </div>
            <div class="col-md-4">
                <label for=""> Distancia: <span class="obligatorio">(Campo Requerido)</span></label>
                <br>
                <input type="number" placeholder="Ingresar la distancia del Planeta" class="form-control" required name="dista_pla_recup_ba" value="<?php echo $editaPlan->dista_pla_recup_ba; ?>">
                <br>
            </div>
            <div class="col-md-4">
                <label for=""> Estado: <span class="obligatorio">(Campo Requerido)</span></label>
                <br>
                <input type="text" placeholder="Ingresar el estado del Planeta" class="form-control" required name="esta_pla_recup_ba" value="<?php echo $editaPlan->esta_pla_recup_ba; ?>">
                <br>
            </div>
            <div class="col-md-4">
                <label for="galaxia_id">Galaxia: </label>
                <select name="galaxia_id" class="form-control" id="galaxia_id" required>
                    <?php foreach ($planetas as $planeta) : ?>
                        <option value="<?php echo $planeta->id_recup_ba; ?>"><?php echo $planeta->nom_recup_ba ?></option>
                    <?php endforeach; ?>
                </select>
            </div>

            <br>
            <div class="col-md-12 text-center">
                <button type="submit" name="button" class="btn btn-primary">
                    GUARDAR
                </button>
                &nbsp;
                <a href="<?php echo site_url(); ?>/planetas/listPla" class="btn btn-danger">CANCELAR</a>
            </div>

</form>

<script type="text/javascript">
    $("#frm_editar_planeta").validate({
        rules: {
            nom_pla_recup_ba: {
                required: true,
                minlength: 3,
                maxlength: 30,
                letras: true,
            },
            orden_pla_recup_ba: {
                required: true,
                minlength: 1,
                maxlength: 3,
                digits: true,
            },
            foto_pla_recup_: {
                required: true,
                minlength: 5,
                maxlength: 250,
                letras: true,
            },
            dista_pla_recup_ba: {
                required: true,
                minlength: 5,
                maxlength: 20,
                digits: true,
            },
            esta_pla_recup_ba: {
                required: true,
                minlength: 5,
                maxlength: 100,
                letras: true,
            }
        },
        messages: {
            nom_pla_recup_ba: {
                required: "Por favor ingresar un nombre",
                minlength: "El nombre debe tener al menos 3 caracteres",
                maxlength: "Nombre incorrecto",

            },
            orden_pla_recup_ba: {
                required: "Por favor ingrese el Orden del planeta",
                minlength: "El Orden debe tener al menos 1 digito",
                maxlength: "Orden Incorrecta",
                digits: "Este campo solo acepta números",
                number: "Este campo solo acepta números",
            },
            foto_pla_recup_: {
                required: "Por favor ingresar la ciudad",
                minlength: "El apellido debe tener al menos 3 caracteres",
                maxlength: "Nombre incorrecto",
            },
            dista_pla_recup_ba: {
                required: "Por favor ingresa un número de distancia",
                minlength: "Distancia incorrecta ingrese al menos 5 digitos",
                maxlength: "Distancia incorrecta",
                digits: "Este campo solo acepta números",
                number: "Este campo solo acepta números",
            },
            esta_pla_recup_ba: {
                required: "Por favor ingresa un estado",
                minlength: "El estado debe tener al menos 5 caracteres",
                maxlength: "Estado muy extenso muy Extensa",

            }
        }
    });
</script>