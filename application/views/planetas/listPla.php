<h1 Align="center">Listar Planetas</h1>

<?php if($planetas): ?>
    <table class="table table-striped text-center" id="tbl_planetas" >
        <thead class="text-center">
            <tr>
                <th>ID</th>
                <th>NOMBRE</th>
                <th>ORDEN</th>
                <th>FOTO</th>
                <th>DISTANCIA</th>
                <th>ESTADO</th>
                <th>GALAXIA</th>
                <th>ACCIONES</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($planetas as $filaTemporal):?>
                <tr >
                    <td>
                        <?php echo $filaTemporal->id_pla_recup_ba; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->nom_pla_recup_ba; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->orden_pla_recup_ba; ?>
                    </td>
                    <td>
                        <?php if ($filaTemporal->foto_pla_recup_ba != "") : ?>
                            <a href="<?php echo base_url('uploads/') . $filaTemporal->foto_pla_recup_ba; ?>" target="_blank">
                                <img src="<?php echo base_url('uploads/') . $filaTemporal->foto_pla_recup_ba; ?>" alt="" width="100px">
                            </a>
                        <?php else : ?>
                            <img src="<?php echo base_url('assets/images/sinImagen.png') ?>" alt="">
                        <?php endif; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->dista_pla_recup_ba; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->esta_pla_recup_ba; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->galaxia_id; ?>
                    </td>

                    <td >
                        <a href="<?php echo site_url(); ?>/planetas/editaPla/<?php echo $filaTemporal->id_pla_recup_ba; ?>" title="Editar Galaxia">
                            <i><img src="<?php echo base_url('assets/images/pencil.png')?>" alt=""></i>
                        </a>
                        &nbsp;&nbsp;&nbsp;
                        <a href="<?php echo site_url();?>/Planetas/eliminaPla/<?php echo $filaTemporal->id_pla_recup_ba; ?>" title="Eliminar Galaxia">
                            <i><img src="<?php echo base_url('assets/images/trash.png') ?>" alt=""></i>
                        </a>
                    </td>
                </tr>
            <?php endforeach;?>
        </tbody>
    </table>
<?php else : ?>
    <h1>NO POSEE PLANETAS ._.</h1>
<?php endif; ?>

<script type="text/javascript">
    $("#tbl_planetas").DataTable();
</script>
