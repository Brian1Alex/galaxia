<?php
class Planetas extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Planeta');
    }

    public function nuePla()
    {
        $datos['planetas'] = $this->Planeta->obteGala();
        $this->load->view('header');
        $this->load->view('planetas/nuePla', $datos);
        $this->load->view('footer');
    }

    public function listPla()
    {
        $data['planetas'] = $this->Planeta->obtenerPla();
        $this->load->view('header');
        $this->load->view('planetas/listPla', $data);
        $this->load->view('footer');
    }

    public function editaPla($id_pla_recup_ba)
    {
        $dat['planetas'] = $this->Planeta->obteGala();
        $dat['editaPlan'] = $this->Planeta->obtenerPorId($id_pla_recup_ba);
        $this->load->view('header');
        $this->load->view('planetas/editaPla', $dat);
        $this->load->view('footer');
    }



    public function guardaPla()
    {
        $datosNuePla = array(
            "nom_pla_recup_ba" => $this->input->post('nom_pla_recup_ba'),
            "orden_pla_recup_ba" => $this->input->post('orden_pla_recup_ba'),
            "dista_pla_recup_ba" => $this->input->post('dista_pla_recup_ba'),
            "esta_pla_recup_ba" => $this->input->post('esta_pla_recup_ba'),
            "galaxia_id" => $this->input->post('galaxia_id')
        );
        $this->load->library("upload");
        $new_name = "foto_pla_" . time() . "_" . rand(1, 5000);
        $config['file_name'] = $new_name . '_1';
        $config['upload_path']          = FCPATH . 'uploads/';
        $config['allowed_types']        = 'jpeg|jpg|png';
        $config['max_size']             = 1024 * 5;
        $this->upload->initialize($config);

        if ($this->upload->do_upload("foto_pla_recup_ba")) {
            $dataSubida = $this->upload->data();
            $datosNuePla["foto_pla_recup_ba"] = $dataSubida['file_name'];
        }

        if ($this->Planeta->insertar($datosNuePla)) {
            redirect('planetas/listPla');
        } else {
            "<h1>Error al ingresar Datos :(</h1>";
        }
    }

    public function eliminaPla($id_pla_recup_ba)
    {
        if ($this->Planeta->borrar($id_pla_recup_ba)) {
            redirect('planetas/listPla');
        } else {
            "<h1>Error al eliminar ._.</h1>";
        }
    }

    public function editarPla()
    {
        $datosEditados = array(
            "nom_pla_recup_ba" => $this->input->post('nom_pla_recup_ba'),
            "orden_pla_recup_ba" => $this->input->post('orden_pla_recup_ba'),
            "foto_pla_recup_ba" => $this->input->post('foto_pla_recup_ba'),
            "dista_pla_recup_ba" => $this->input->post('dista_pla_recup_ba'),
            "esta_pla_recup_ba" => $this->input->post('esta_pla_recup_ba'),
            "galaxia_id" => $this->input->post('galaxia_id')
        );
        $id_pla_recup_ba = $this->input->post("id_pla_recup_ba");
        if ($this->Planeta->editar($id_pla_recup_ba, $datosEditados)) {
            redirect('planetas/listPla');
        }
    }
}
