<?php
    class Galaxias extends CI_Controller
    {
        function __construct()
        {
            parent::__construct();
            $this->load->model('Galaxia');
        }

        public function nuegala()
        {
            $this->load->view('header');
            $this->load->view('galaxias/nuegala');
            $this->load->view('footer');
        }

        public function lisgala()
        {
            $data['galaxias'] = $this->Galaxia->obtenerGala();
            $this->load->view('header');
            $this->load->view('galaxias/lisgala', $data);
            $this->load->view('footer');
        }

        public function editaGala($id_recup_ba)
        {
            $dat['editaGalax'] = $this->Galaxia->obtenerPorId($id_recup_ba);
            $this->load->view('header');
            $this->load->view('galaxias/editaGala', $dat);
            $this->load->view('footer');
        }

        public function guardaGala()
        {
            $datosNueGala = array(
                "nom_recup_ba" => $this->input->post('nom_recup_ba'),
                "descrip_recup_ba" => $this->input->post('descrip_recup_ba')
            );
            if ($this->Galaxia->insertar($datosNueGala)) {
                redirect('galaxias/lisGala');
            } else {
                "<h1>Error al ingresar Datos :(</h1>";
            }
            
        }

        public function eliminaGala($id_recup_ba)
        {
            if ($this->Galaxia->borrar($id_recup_ba)) {
                redirect('galaxias/lisgala');
            }else{
                "<h1>Error al eliminar ._.</h1>";
            }
        }

        public function editarGalax()
        {
            $datosEditados = array(
                "nom_recup_ba" => $this->input->post('nom_recup_ba'),
                "descrip_recup_ba" => $this->input->post('descrip_recup_ba')
            );
            $id_recup_ba = $this->input->post("id_recup_ba");
            if ($this->Galaxia->editar($id_recup_ba, $datosEditados)) {
                redirect('galaxias/lisgala');
            }
        }

    }